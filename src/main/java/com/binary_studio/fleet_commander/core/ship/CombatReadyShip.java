package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static java.lang.Math.min;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip ship;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger speed;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.shieldHP = ship.getShieldHP();
		this.hullHP = ship.getHullHP();
		this.capacitorAmount = ship.getCapacitorAmount();
		this.speed = PositiveInteger.of(0);
	}

	@Override
	public void endTurn() {
		// at the turn end ship stops, i.e. speed equals 0
		this.speed = PositiveInteger.of(0);
		this.capacitorAmount = PositiveInteger
				.of(min(this.capacitorAmount.value() + this.ship.getCapacitorRechargeRate().value(),
						this.ship.getCapacitorAmount().value()));
	}

	@Override
	public void startTurn() {
		// at the turn start ship moves, i.e. speed above 0
		this.speed = this.ship.getSpeed();
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() < this.ship.getAttackSubsystem().getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		AttackSubsystem sub = this.ship.getAttackSubsystem();
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() - this.ship.getAttackSubsystem().getCapacitorConsumption().value());
		return Optional.of(new AttackAction(sub.attack(target), this::getName, target, sub));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this.ship.getDefenciveSubsystem().reduceDamage(attack);
		if (this.shieldHP.value() < attack.damage.value()) {
			int totalDamage = attack.damage.value();
			totalDamage -= this.shieldHP.value();
			this.shieldHP = PositiveInteger.of(0);
			if (this.hullHP.value() < totalDamage) {
				this.hullHP = PositiveInteger.of(0);
			}
			else {
				this.hullHP = PositiveInteger.of(this.hullHP.value() - totalDamage);
			}
		}
		else {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - attack.damage.value());
		}
		return this.shieldHP.value() + this.hullHP.value() == 0 ? new AttackResult.Destroyed()
				: new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		DefenciveSubsystemImpl defSys = (DefenciveSubsystemImpl) this.ship.getDefenciveSubsystem();
		if (this.capacitorAmount.value() < defSys.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		// calculate final values of shield and hull after regeneration
		int shield = min(this.shieldHP.value() + defSys.getShieldRegeneration().value(),
				this.ship.getShieldHP().value());
		int hull = min(this.hullHP.value() + defSys.getHullRegeneration().value(), this.ship.getHullHP().value());
		// spend the charge
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() - defSys.getCapacitorConsumption().value());
		RegenerateAction deltaCharge = new RegenerateAction(PositiveInteger.of(shield - this.shieldHP.value()),
				PositiveInteger.of(hull - this.hullHP.value()));
		this.shieldHP = PositiveInteger.of(shield); // regenerating hp-s
		this.hullHP = PositiveInteger.of(hull);
		return Optional.of(deltaCharge);
	}

}
