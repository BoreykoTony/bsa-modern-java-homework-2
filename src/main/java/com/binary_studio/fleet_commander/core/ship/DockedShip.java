package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger pg;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.pg = pg;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		// calculate the current consumption of both modules
		final int attackSubPGConsume = subsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
		final int defenceSubPGConsume = this.defenciveSubsystem == null ? 0
				: this.defenciveSubsystem.getPowerGridConsumption().value();
		if (attackSubPGConsume + defenceSubPGConsume <= this.pg.value()) {
			this.attackSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(attackSubPGConsume + defenceSubPGConsume - this.pg.value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		final int attackSubPGConsume = this.attackSubsystem == null ? 0
				: this.attackSubsystem.getPowerGridConsumption().value();
		final int defenceSubPGConsume = subsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
		if (attackSubPGConsume + defenceSubPGConsume <= this.pg.value()) {
			this.defenciveSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(attackSubPGConsume + defenceSubPGConsume - this.pg.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem != null && this.attackSubsystem != null) {
			return new CombatReadyShip(this);
		}
		else if (this.defenciveSubsystem != null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem != null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else {
			throw NotAllSubsystemsFitted.bothMissing();
		}
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

}
