package com.binary_studio.fleet_commander.core.common;

public final class NameValidator {

	private NameValidator() {
	}

	public static boolean isCorrect(String name) {
		// more rules can be added
		if (name == null || name.trim().isEmpty()) {
			return false;
		}
		return true;
	}

}
