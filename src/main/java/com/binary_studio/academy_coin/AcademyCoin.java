package com.binary_studio.academy_coin;

import java.util.stream.Stream;

import static java.lang.Math.*;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		Integer[] p = prices.toArray(Integer[]::new);
		if (p.length == 0) {
			return 0;
		}
		int buying = 0;
		int selling = -p[0];
		for (int i = 1; i < p.length; i++) {
			buying = max(buying, selling + p[i]);
			selling = max(selling, buying - p[i]);
		}
		return buying;
	}

}
