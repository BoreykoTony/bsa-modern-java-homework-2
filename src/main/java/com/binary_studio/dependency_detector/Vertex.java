package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

	private List<Vertex> neighbors;

	private int color;

	public Vertex() {
		this.color = 0;
		this.neighbors = new ArrayList<>();
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getColor() {
		return this.color;
	}

	public List<Vertex> getNeighbors() {
		return this.neighbors;
	}

}
