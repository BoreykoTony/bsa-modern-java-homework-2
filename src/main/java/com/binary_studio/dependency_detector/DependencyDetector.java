package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.HashMap;

public final class DependencyDetector {

	private static ArrayList<Vertex> graph;

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		graph = new ArrayList<>();
		HashMap<String, Integer> nameToId = new HashMap<>();
		for (int i = 0; i < libraries.libraries.size(); ++i) {
			graph.add(new Vertex());
			nameToId.put(libraries.libraries.get(i), i);
		}
		for (String[] i : libraries.dependencies) {
			graph.get(nameToId.get(i[0])).getNeighbors().add(graph.get(nameToId.get(i[1])));
		}
		for (Vertex vertex : graph) {
			if (vertex.getColor() == 0 && ds(vertex)) {
				return false;
			}
		}
		return true;
	}

	// deep search in graph
	private static boolean ds(Vertex vertex) {
		vertex.setColor(1);
		for (Vertex i : vertex.getNeighbors()) {
			if (i.getColor() == 0 && ds(i)) {
				return true;
			}
			if (i.getColor() == 1) {
				return true;
			}
		}
		vertex.setColor(2);
		return false;
	}

}
